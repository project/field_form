<?php

/**
* @file
* Views access plugin implemented for the Field form module.
*/

/**
 * Access plugin that provides custom callback permission-based access control.
 */
class field_form_plugin_access_callback extends views_plugin_access {
  function access($account) {
    return field_form_access_callback($this->options['access_callback'], $this->options['access_arguments'], $account);
  }

  function get_access_callback() {
    return array('field_form_access_callback', array($this->options['access_callback'], $this->options['access_arguments']));
  }

  function summary_title() {
    return t('@callback(@arguments)', array('@callback' => $this->options['access_callback'], '@arguments' => $this->options['access_arguments']));
  }


  function option_definition() {
    $options = parent::option_definition();
    $options['access_callback'] = array('default' => 'user_access');
    $options['access_arguments'] = array('default' => 'access content');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['access_callback'] = array(
      '#type' => 'textfield',
      '#size' => 30,
      '#title' => t('Callback function'),
      '#default_value' => $this->options['access_callback'],
      '#description' => t('A function returning a boolean value that determines whether the user has access rights to this view.'),
    );
    $form['access_arguments'] = array(
      '#type' => 'textfield',
      '#size' => 30,
      '#title' => t("Callback function's arguments"),
      '#default_value' => $this->options['access_arguments'],
      '#description' => t('Arguments to pass to the access callback function separated with comma ",". For path component substitution, you can use percent substitution here to replace with arguments. Use "%1" for the first argument, "%2" for the second, etc.'),
    );
  }
}
