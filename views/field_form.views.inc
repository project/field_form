<?php

/**
* @file
* Views hooks implemented for the Field form module.
*/

/**
 * Implement hook_views_data().
 */
function field_form_views_data() {
  // Table definition for Field form's submissions
  $data['field_form_results']['table']['group'] = t('Field form submissions');
  $data['field_form_results']['table']['base'] = array(
    'field' => 'uid',
    'title' => t('Field form submissions'),
    'help'  => t("User's Field form submission results."),
  );
  // Joins
  $data['field_form_results']['table']['join'] = array(
    //...to the node table
    'node' => array(
      'table'      => 'field_form_results',
      'left_field' => 'nid',
      'field'      => 'entity_id',
      'extra'      => array(
        array(
          'table'   => 'field_form_results',
          'field'   => 'etid',
          'value'   => _field_sql_storage_etid('node'),
          'numeric' => TRUE,
        ),
      ),
    ),
    //...to the users table
    'users' => array(
      'table'      => 'field_form_results',
      'left_field' => 'uid',
      'field'      => 'entity_id',
      'extra'      => array(
        array(
          'table'   => 'field_form_results',
          'field'   => 'etid',
          'value'   => _field_sql_storage_etid('user'),
          'numeric' => TRUE,
        ),
      ),      
    ),
    //...to the taxonomy_term_data table
    'term_data' => array(
      'table'      => 'field_form_results',
      'left_field' => 'tid',
      'field'      => 'entity_id',
      'extra'      => array(
        array(
          'table'   => 'field_form_results',
          'field'   => 'etid',
          'value'   => _field_sql_storage_etid('taxonomy_term'),
          'numeric' => TRUE,
        ),
      ),      
    ),
  );
  // User ID
  $data['field_form_results']['uid'] = array(
    'title' => t('Owner'),
    'help'  => t('The users who submitted these submissions.'),
    'relationship' => array(
      'base'      => 'users',
      'field'     => 'uid',
      'label'     => t('Submitted by'),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );
  // Entity type ID
  $data['field_form_results']['etid'] = array(
    'title' => t('Entity type ID'),
    'help'  => t('Unique ID of entity type eg. node, user assigned by field module. For views entity will always have a value of 0.'),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );
  // Entity ID
  $data['field_form_results']['entity_id'] = array(
    'title' => t('Entity ID'),
    'help'  => t('The entity type ID this submission is attached to.'),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );
  // Entity type
  $data['field_form_results']['entity_type'] = array(
    'title' => t('Entity type'),
    'help'  => t('An entity type.'),
    'field' => array(
      'handler'        => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  // User name
  $data['field_form_results']['user_name'] = array(
    'title' => t('User name'),
    'help'  => t('User name of submission owner.'),
    'field' => array(
      'handler'        => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  // IP address
  $data['field_form_results']['remote_addr'] = array(
    'title' => t('IP address'),
    'help'  => t("Submission owner's IP address."),
    'field' => array(
      'handler'        => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  // Source title
  $data['field_form_results']['title'] = array(
    'title' => t('Source title'),
    'help'  => t("Source entity's title this Field form is attached to."),
    'field' => array(
      'handler'        => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  // Source path
  $data['field_form_results']['path'] = array(
    'title' => t('Source path'),
    'help'  => t("Source entity's path this Field form is attached to."),
    'field' => array(
      'handler'        => 'views_handler_field_url',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  // Score
  $data['field_form_results']['score'] = array(
    'title' => t('Score'),
    'help'  => t("User's score."),
    'field' => array(
      'handler'        => 'views_handler_field_numeric',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
	// Retries
  $data['field_form_results']['retries'] = array(
    'title' => t('Retries'),
    'help' 	=> t('Number of times the user submitted the form.'),
    'field' => array(
      'handler' 			 => 'views_handler_field_numeric',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  // Duration
  $data['field_form_results']['duration'] = array(
    'title' => t('Duration'),
    'help' 	=> t('Time consumed by the user to complete the Field form.'),
    'field' => array(
      'handler' 			 => 'field_form_handler_field_time_interval',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  // Timestamp
  $data['field_form_results']['timestamp'] = array(
    'title' => t('Timestamp'),
    'help'  => t('Field form submission timestamp.'),
    'field' => array(
      'handler'        => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  // View link
  $data['field_form_results']['view_submission'] = array(
    'field' => array(
      'title'     => t('View link'),
      'help'      => t('Provide a simple link to view the submission.'),
      'handler'   => 'field_form_handler_submission_link',
      'link_type' => 'view',
    ),
  );
  // Edit link
  $data['field_form_results']['edit_submission'] = array(
    'field' => array(
      'title'     => t('Edit link'),
      'help'      => t('Provide a simple link to edit the submission.'),
      'handler'   => 'field_form_handler_submission_link',
      'link_type' => 'edit',
    ),
  );

  // Delete link
  $data['field_form_results']['delete_submission'] = array(
    'field' => array(
      'title'     => t('Delete link'),
      'help'      => t('Provide a simple link to delete the submission.'),
      'handler'   => 'field_form_handler_submission_link',
      'link_type' => 'delete',
    ),
  );
  return $data;
}

/**
 * Implementation of hook_views_data_alter().
 */
function field_form_views_data_alter(&$data) {
  if (isset($data['field_data_taxonomy_field_form'])) {
    // Join the Field form's taxonomy term table
    $data['field_data_taxonomy_field_form']['table']['join']['field_form_results'] = array(
      'left_table' => 'field_form_results',
      'left_field' => 'etid',
      'table'      => 'field_data_taxonomy_field_form',
      'field'      => 'etid',
      'extra'      => '{field_form_results}.entity_id = {field_data_taxonomy_field_form}.entity_id AND {field_form_results}.language = {field_data_taxonomy_field_form}.language AND {field_data_taxonomy_field_form}.deleted = 0 AND {field_data_taxonomy_field_form}.delta = 0',
    ); 
    // Term
    $data['field_data_taxonomy_field_form']['taxonomy_field_form_tid']['group']  = t('Field form term');
    $data['field_data_taxonomy_field_form']['taxonomy_field_form_tid']['title']  = t('Term');
    $data['field_data_taxonomy_field_form']['taxonomy_field_form_tid']['help']   = t("Field form's taxonomy term where a content is associated.");
    $data['field_data_taxonomy_field_form']['taxonomy_field_form_tid']['filter'] = array('handler' => 'field_form_handler_filter_term');
  }
}

/**
 * Implementation of hook_views_handlers().
 */
function field_form_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'field_form') . '/views',
    ),
    'handlers' => array(
      'field_form_handler_submission_link' => array(
        'parent' => 'views_handler_field',
        'file' => 'field_form_handler_submission_link.inc',
      ),
      'field_form_handler_field_time_interval' => array(
        'parent' => 'views_handler_field',
        'file' => 'field_form_handler_field_time_interval.inc',
      ),
      'field_form_handler_filter_term' => array(
        'parent' => 'views_handler_filter_in_operator',
        'file' => 'field_form_handler_filter_term.inc',
      ),
      'field_form_plugin_access_callback' => array(
        'parent' => 'views_plugin_access',
        'file' => 'field_form_plugin_access_callback.inc',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_plugins_alter().
 */
function field_form_views_plugins_alter(&$plugins) {
  $plugins['access']['field_form_access_callback'] = array(
    'title' => t('Access callback'),
    'help' => t('Access will be granted to users with the specified access callback function and its arguments.'),
    'handler' => 'field_form_plugin_access_callback',
    'uses options' => TRUE,
    'help topic' => 'access-perm',
  );
}