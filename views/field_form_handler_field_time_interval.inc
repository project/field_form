<?php
/**
* @file
* Views handler to display duration of a submission in human readable format.
*/

/**
 * Field handler to present human readable format duration to the user.
 */
class field_form_handler_field_time_interval extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }
  
  function render($values) {
    $value = $values->{$this->field_alias};
    if ($this->options['hide_empty'] && empty($value) && ($value !== 0 || $this->options['empty_zero'])) {
      return '';
    }
    
    return format_interval($value, 1);
  }
}