<?php
/**
* @file
* Views handler to display links to a submission.
*/

/**
 * Field handler to present a link to the user.
 */
class field_form_handler_submission_link extends views_handler_field {
  var $link_type;

  function construct() {
    // We need to set this property before calling the construct() chain
    // as we use it in the option_definintion() call.
    $this->link_type = $this->definition['link_type'];

    parent::construct();
    $this->additional_fields['uid'] = 'uid';
    $this->additional_fields['etid'] = 'etid';
    $this->additional_fields['entity_id'] = 'entity_id';
  }

  function allow_advanced_render() {
    return FALSE;
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['label'] = array('default' => '', 'translatable' => TRUE);
    $options['text']  = array('default' => $this->link_type, 'translatable' => TRUE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text to display'),
      '#default_value' => $this->options['text'],
    );
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $_SESSION['field_form']['redirect_path'] = request_path();
    $uid       = $values->{$this->aliases['uid']};
    $etid      = $values->{$this->aliases['etid']};
    $entity_id = $values->{$this->aliases['entity_id']};
    $text      = !empty($this->options['text']) ? $this->options['text'] : t('@op', array('@op' => $this->link_type));
    return l(t('@text', array('@text' => $text)), "field-form/$uid/$etid/$entity_id/$this->link_type");
  }
}