<?php
/**
* @file
* Views handler to filter submission according to its taxonomy term.
*/

/**
 * Filter by submission's taxonomy term.
 */
class field_form_handler_filter_term extends views_handler_filter_in_operator {
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Class');      
      $vid = variable_get('field_form_vocabulary', 0);
      if ($vid) {
        $terms = taxonomy_term_load_multiple(FALSE, array('vid' => $vid));
        foreach ($terms as $tid => $object) {
          $options[$tid] = t('@name', array('@name' => $object->name));
        }
      }
      else {
        $options = array();
      }      
      $this->value_options = $options;
    }
  }
}