<?php

/**
 * Callback function of the path 
 * field-form/(uid)/(etid)/(entity_id)/view
 *
 * @param $submission
 *   An associative array containing submission result information: uid, 
 *   etid, entity_id, entity_type, language, user_name, remote_addr, title, 
 *   path, score, retries, duration, timestamp and submission (serialized 
 *   renderable array).
 *
 * @return
 *   Rendered HTML of a Field form submission's view mode
 */
function field_form_view_submission($submission) {
  drupal_set_title(t('Submission entry of !user at !title.', array('!user' => $submission['user_name'], '!title' => $submission['title'])));
  $submission['view']       = TRUE;
  $submission['submission'] = unserialize($submission['submission']);
  return theme('field_form_submission_details', array('submission' => $submission));
}

/**
 * Callback function of the path 
 * field-form/(uid)/(etid)/(entity_id)/edit
 *
 * @param $submission
 *   An associative array containing submission result information: uid, 
 *   etid, entity_id, entity_type, language, user_name, remote_addr, title, 
 *   path, score, retries, duration, timestamp and submission (serialized 
 *   renderable array).
 *
 * @return
 *   Rendered HTML of a Field form submission's edit mode
 */
function field_form_edit_submission($submission) {
  drupal_set_title(t("Edit !user's submission entry at !title.", array('!user' => $submission['user_name'], '!title' => $submission['title'])));
  $form   = unserialize($submission['submission']);
  foreach (element_children($form) as $field_name) {
    unset($form[$field_name]['#printed']);
    unset($form[$field_name]['#defaults_loaded']);
    unset($form[$field_name]['#processed']);
    unset($form[$field_name]['#id']);
    unset($form[$field_name]['#name']);
    unset($form[$field_name]['#sorted']);
    unset($form[$field_name]['#needs_validation']);
    unset($form[$field_name]['#process']);
    unset($form[$field_name]['#theme']);
    unset($form[$field_name]['#theme_wrappers']);
    unset($form[$field_name]['#tree']);
    unset($form[$field_name]['#parents']);
    unset($form[$field_name]['#array_parents']);
    unset($form[$field_name]['#default_value']);
    if ($form[$field_name]['#field_form_input_type'] == 'textarea') {
      $form[$field_name]['#default_value'] = $form[$field_name]['value']['#value'];
    }
    else {
      $form[$field_name]['#default_value'] = $form[$field_name]['#value']; 
    }
    unset($form[$field_name]['#value']);
    foreach (element_children($form[$field_name]) as $field_child) {
      unset($form[$field_name][$field_child]);
    }
  }
  $form['submit_info'] = array(
    '#type'   => 'markup',
    '#markup' => theme('field_form_submission_details', array('submission' => $submission)),
    '#weight' => -10,
  );
  $form['submit_button'] = array(
    '#type'                 => 'submit',
    '#value'                => t('Update'),
    '#field_form_edit'      => TRUE,
    '#field_form_etid'      => $submission['etid'],
    '#field_form_entity'    => $submission['entity_type'],
    '#field_form_entity_id' => $submission['entity_id'],
    '#field_form_uid'       => $submission['uid'],
  );
  return drupal_get_form('field_form_user_form', $form);
}

/**
 * Callback function of the path 
 * field-form/(uid)/(etid)/(entity_id)/delete
 *
 * @param $form 
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form. 
 * @param $submission
 *   An associative array containing submission result information: uid, 
 *   etid, entity_id, entity_type, language, user_name, remote_addr, title, 
 *   path, score, retries, duration, timestamp and submission (serialized 
 *   renderable array).
 *
 * @return
 *   Rendered HTML of a confirmation for Field form submission deletion
 */
function field_form_delete_submission($form, &$form_state, $submission) {
  $form_state['storage']['uid']       = $submission['uid'];
  $form_state['storage']['etid']      = $submission['etid'];
  $form_state['storage']['entity_id'] = $submission['entity_id'];
  $form['submit_info'] = array(
    '#type'   => 'markup',
    '#markup' => theme('field_form_submission_details', array('submission' => $submission)),
    '#weight' => -10,
  );
  return confirm_form($form,
    t("Are you sure you want to permanently delete the %user's submission entry at %title?", array('%user' => $submission['user_name'], '%title' => $submission['title'])),
    isset($_SESSION['field_form']['redirect_path']) ? $_SESSION['field_form']['redirect_path'] : 'field-form',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );

}

/**
 * Callback submit function for field_form_delete_submission()
 *
 * @param $form 
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 * @see field_form_delete_submission()
 */
function field_form_delete_submission_submit($form, &$form_state) {
  db_delete('field_form_results')
  ->condition('uid', $form_state['storage']['uid'])
  ->condition('etid', $form_state['storage']['etid'])
  ->condition('entity_id', $form_state['storage']['entity_id'])
  ->execute(); 
  drupal_set_message(t('Delete successful.'));
  $form_state['redirect'] = isset($_SESSION['field_form']['redirect_path']) ? $_SESSION['field_form']['redirect_path'] : 'field-form';
}