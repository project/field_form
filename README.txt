
Description:
Field form module adds input form elements as field to any fieldable entities such
as node, user and taxonomy term to your Drupal site. Text field, radio buttons,
check boxes, select list (single/multi selection), text area and date are the 
available input form elements for Fields in Core. Field form can be used as 
questionnaire or inquiry that asks for a reply. Submissions are stored in a database 
and are presented as table using the powerful data presentation feature of the
Views module. The event of upon Field form submission of a user is tied to 
Trigger module which can be associated with system action such as 
"Display a message to the user...", "Send e-mail..." and "Redirect to URL..." 
with exposed tokens:
[field_form:source:title]
[field_form:source:path]
[field_form:source:link]
[field_form:score]
[field_form:retries]
[field_form:duration]
[field_form:timestamp]
[field_form:user:name]
[field_form:user:ip]
[field_form:user:mail]
[field_form:submission:path]
[field_form:submission:link]
[field_form:subject]
[field_form:message]
Field form adds additional vocabulary to your taxonomy with one sample term named 
"Class" which can be used to group, sort or filter in Views module.

Requirements:
Drupal 7.x
Taxonomy
Views 7.x-3.x

Installation:
1. Copy the extracted field_form directory to your Drupal sites/all/modules directory.
2. Login as an administrator. Enable the module at http://www.example.com/admin/modules 
   as well as the other required modules.
3. Add Field form input element(s) to node under http://www.example.com/admin/structure/types
   and select "Manage Fileds". Basically any entities that are fieldable can add Field form
   element(s) under their "Manage Fileds".

Support:
Please use the issue queue for filing bugs with this module at
http://drupal.org/project/issues/field_form