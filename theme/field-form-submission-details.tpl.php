<?php

/**
 * @file field-form-submission-details.tpl.php
 * Default theme implementation for rendering user's submission result details.
 *
 * Available variables:
 * - $submission: An associative array containing:
 * -- submission: user's submission result details - user_name, entity_type, 
 *    remote_addr, title, path, score, retries, timestamp and submission 
 *    (renderable array).
 *
 * @ingroup themeable
 */
drupal_add_css(drupal_get_path('module', 'field_form') . '/css/field_form.css', array('weight' => CSS_THEME, 'preprocess' => FALSE));
?>
<h3><?php print t('Submission details'); ?></h3>
<p><?php print l(t('Back to list'), isset($_SESSION['field_form']['redirect_path']) ? $_SESSION['field_form']['redirect_path'] : 'field-form'); ?></p>
<dl class="field-form-submit-info">
<dt><?php print t('Submitted by:'); ?></dt>
<dd><?php print l($submission['user_name'], 'user/' . $submission['uid']); ?></dd>
<dt><?php print t('IP address:'); ?></dt>
<dd><?php print $submission['remote_addr']; ?></dd>
<dt><?php print t('Timestamp:'); ?></dt>
<dd>
<?php
  date_default_timezone_set(variable_get('date_default_timezone', 'UTC'));
  print date(variable_get('date_format_medium', 'D, Y-m-d H:i'), $submission['timestamp']); 
?>
</dd>
<dt><?php print t('Source page:'); ?></dt>
<dd><?php print l(t('@title', array('@title' => $submission['title'])), $submission['path']); ?></dd>
</dl>
<?php if (isset($submission['view'])): ?>
<dl class="field-form-submit-result">
  <?php foreach ($submission['submission'] as $field_name => $field): ?>
    <dt><?php print t('@title', array('@title' => $field['#title'])); ?></dt>
    <dd>
      <?php if ($field['#field_form_input_type'] == 'textfield'): ?>
        <p class="field-form-answer">
          <?php print $field['#value'] ? t('@value', array('@value' => $field['#value'])) : t('- %empty -', array('%empty' => 'Empty')); ?>
        </p>
      <?php elseif ($field['#field_form_input_type'] == 'textarea'): ?>
        <div class="field-form-answer">
          <?php print $field['value']['#value'] ? check_markup($field['value']['#value'], $field['format']['format']['#value'], $submission['language']) : t('- %empty -', array('%empty' => 'Empty')); ?>
        </div>
      <?php elseif ($field['#field_form_input_type'] == 'select' || $field['#field_form_input_type'] == 'radios'): ?>
        <p class="field-form-answer">
          <?php print isset($field['#options'][$field['#value']]) ? t('@value', array('@value' => $field['#options'][$field['#value']])) : t('- %empty -', array('%empty' => 'Empty')); ?>
        </p>
      <?php elseif ($field['#field_form_input_type'] == 'selectmulti' || $field['#field_form_input_type'] == 'checkboxes'): ?>
        <div class="field-form-answer">
          <?php if (empty($field['#value'])): ?>
            <?php print t('- %empty -', array('%empty' => 'Empty')); ?>
          <?php else: ?>
            <ul>
            <?php foreach ($field['#value'] as $value): ?>
              <li><?php print $field['#options'][$value] ?></li>
            <?php endforeach; ?>
            </ul>
          <?php endif; ?>
        </div>
      <?php elseif ($field['#field_form_input_type'] == 'date'): ?>
        <p class="field-form-answer">
          <?php 
            $date = mktime(0, 0, 0, $field['#value']['month'], $field['#value']['day'], $field['#value']['year']);
            print $date ? t('@value', array('@value' => date('M-j-Y', $date))) : t('- %empty -', array('%empty' => 'Empty')); 
          ?>
        </p>
      <?php endif; ?>
      <?php print isset($field['#suffix']) ? $field['#suffix'] : ''; ?>    
    </dd>
  <?php endforeach; ?>
</dl>
<?php endif; ?>