
(function ($) {
  Drupal.behaviors.fieldForm = {
    attach: function (context, settings) {
      $('fieldset.field-form-settings', context).drupalSetSummary(function (context) {
				var values = new Array();
        var t = new Array();
        var perm = new Array();
        var lang = settings.field_form.language;
        // Show status of the selected Class
        t['%class'] = $('#edit-taxonomy-field-form-' + lang + ' option[selected=true]', context).text();
        values.push(Drupal.t('Class=%class', t));
        // Show status of the enforced limit
				if ($('#edit-enforce-limit-no', context).attr('checked')) {
          values.push(Drupal.t('Unlimitted submission'));
        }
        else {          
          t['%limit']    = $('#edit-submit-limit', context).attr('value');
          t['@interval'] = $('#edit-submit-interval option[selected=true]', context).text();
          values.push(Drupal.t('%limit submission(s) limit @interval', t));          
        }
        // Show status of the label of submit button
        t['@label'] = $('#edit-submit-label', context).attr('value');
        values.push(Drupal.t('"@label"', t));
        // Show status of the Field form access roles
        $('.form-item-access-roles :checkbox:checked:enabled~label', context).text(function(index, text) {
          perm.push(text);
        });
        t['%perm'] = perm.join(', ');
        if (t['%perm']) {
          values.push(Drupal.t('Accessible to: %perm', t));
        }
				return values.join(', ');
			});
      // On click or change, make a parent radio button selected.'
      var setActive = function() {
        $('.form-radio', $(this).parent().parent()).attr('checked', true);
      };
      $('.field-form-set-active', context).click(setActive).change(setActive);
    }
  };
})(jQuery);